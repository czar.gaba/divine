-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2019 at 04:30 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `api`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `ID` int(11) NOT NULL,
  `user` text NOT NULL,
  `pass` text NOT NULL,
  `station_ID` text NOT NULL,
  `access` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`ID`, `user`, `pass`, `station_ID`, `access`) VALUES
(4, 'reggie', 'wew', 'reggie', 'god'),
(5, 'e', 'e', 'e', 'e'),
(6, 'df', 'dfd', 'dfdfdfd', 'fdfdfdfd'),
(7, 'df', 'dfd', 'dfdfdfd', 'fdfdfdfd'),
(8, 'df', 'df', 'df', 'df'),
(9, 'df', 'df', 'df', 'df'),
(10, 'df', 'df', 'df', 'df'),
(11, 'vvvvv', 'vvvv', 'vvvv', 'vvvvvvv'),
(13, 'qwe', 'werwer', '5', 'admin'),
(15, 'Potangina', 'ka', '12', 'hampaslupa'),
(16, 'wew', 'ge', '13', 'user'),
(19, '', '', '34', 'admin'),
(20, 'bobo', 'ka', '12', 'developer'),
(22, 'fd', 'fd', 'df', 'df'),
(23, '3rdgear_47', 'df', '43434', 'dfdfdf'),
(24, 'fdfd', 'dfdf', 'dfdf', 'dfdfd');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gas`
--

CREATE TABLE `tbl_gas` (
  `ID` int(11) NOT NULL,
  `station_ID` text NOT NULL,
  `type` text NOT NULL,
  `price` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_gas`
--

INSERT INTO `tbl_gas` (`ID`, `station_ID`, `type`, `price`) VALUES
(1, 'caggay101', 'XCS', '100'),
(2, 'caggay101', 'diesel', '100'),
(3, 'centro101', 'gold', '100'),
(4, 'centro101', 'silver', '100');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_station`
--

CREATE TABLE `tbl_station` (
  `ID` int(11) NOT NULL,
  `station_ID` text NOT NULL,
  `name` text NOT NULL,
  `longtitude` text NOT NULL,
  `latitude` text NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_station`
--

INSERT INTO `tbl_station` (`ID`, `station_ID`, `name`, `longtitude`, `latitude`, `description`, `image`, `address`) VALUES
(1, 'caggay101', 'Petron Caggay', '17.630873', '121.740146', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"', 'https://placehold.it/500x500', 'caggay highway tuguegarao city'),
(2, 'centro101', 'Shell Bonifacio', '17.617021', '121.728530', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"', 'https://placehold.it//500x500', 'shell bonifacio tuguegarao city');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_gas`
--
ALTER TABLE `tbl_gas`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_station`
--
ALTER TABLE `tbl_station`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tbl_gas`
--
ALTER TABLE `tbl_gas`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_station`
--
ALTER TABLE `tbl_station`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
