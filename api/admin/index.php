<?php include('header.php');?>
<?php include('includes/menu.php');?>
<?php 

    $connection = mysqli_connect("localhost","root","","api");
    
    if(isset($_POST['btn-register']))
    {
        $sid= $_POST['stationid'];
        $sname = $_POST['stationname'];
        
        $slgtd = $_POST['stationlgtd'];
        $slttd = $_POST['stationlttd'];
        $sdes = $_POST['stationdes'];
        // $simage = $_POST['stationimage'];
        $saddress = $_POST['stationaddress'];

        $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        // echo $actual_link;

        $host  = $_SERVER['HTTP_HOST'];
        $host_upper = strtoupper($host);
        $path   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
        $baseurl = "http://" . $host . $path . "/";


        $fileName = $_FILES['stationimage']['name'];
        $tmpName = $_FILES['stationimage']['tmp_name'];
        $fileSize = $_FILES['stationimage']['size'];
        $fileType = $_FILES['stationimage']['type'];
        $filePath =  "./img/" . $fileName;
        $filePath2 =   $baseurl."img/" . $fileName;
        $result = move_uploaded_file($tmpName, $filePath);

    
        $query = "INSERT INTO `tbl_station`(`station_ID`,`name`,`longtitude`,`latitude`,`description`,`image`,`address`) VALUES ('$sid','$sname','$slgtd','$slttd','$sdes','$filePath2','$saddress')";
        $query_run = mysqli_query($connection, $query);
        


        if($query_run)
        {
                    
            ?>
            <script>alert('success');</script>
            <?php
    
        }
    
        else{
            ?>
            <script>alert('fail');</script>
            <?php
        }
    
    
    }

?>

  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">

<!-- Main Content -->
<div id="content">

<?php include('includes/topbar.php');?>

  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Dashboard</h1>
    <h5>
    <?php
     $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
      // echo $actual_link;
?>
    </h5>



    <div class="modal-body">
           
      <!-- Button trigger modal -->
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Add Station</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Fill all the specific fields</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form action="index.php" method="POST" enctype="multipart/form-data">
      <div class="modal-body">

        <div class="form-group">
              <label>Station ID</label>
              <input type="text" name="stationid" class="form-control" placeholder="Enter Station ID" required>
          </div>
          <div class="form-group">
              <label>Station Name</label>
              <input type="password" name="stationname" class="form-control" placeholder="Enter Station name" required>
          </div>
          <div class="form-group">
              <label>Station longtitude</label>
              <input type="text" name="stationlgtd" class="form-control" placeholder="Enter Station longtitude" required>
          </div>
          <div class="form-group">
              <label>Station latitude</label>
              <input type="text" name="stationlttd" class="form-control" placeholder="Enter Station latitude" required>
          </div>
          <div class="form-group">
              <label>Station description</label>
              
              <textarea name="stationdes" class="form-control" id="" cols="30" rows="10"></textarea>
          </div>
          <div class="form-group">
              <label>Station image</label>
              <input id="avatar-1" name="stationimage" type="file" class="file-loading" required>
              
          </div>
          <div class="form-group">
              <label>Station address</label>
              <input type="text" name="stationaddress" class="form-control" placeholder="Enter Station address" required>
          </div>

      </div>
     
          <div class="modal-footer">
             <button type="submit" class="btn btn-primary" value="submit" name="btn-register">Save changes</button>
          </div> 
      </form>
    </div>
  </div>
 </div>
</div>



          <div class="table-responsive">
          <?php
              

               $sql = "SELECT * FROM tbl_station";
               $query_run = mysqli_query($connection, $sql);
          ?>
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                 <thead>
                    <tr>
                        <th> Station ID </th>
                        <th> Station Name </th>
                        <th> Longtitude </th>
                        <th> Latitude </th>
                        <th> Description </th>
                        <!-- <th>image</th> -->
                        <th> Address </th>
                        <th>action</th>
                        
                    </tr>
                 </thead>
                 <tbody>
                 <?php
                 if (mysqli_num_rows($query_run) > 0)
                 {
                     while($row = mysqli_fetch_assoc($query_run))
                    {
                        ?>
                     <tr>
                        <td><?php echo $row['station_ID']; ?></td>
                        <td><?php echo $row['name']; ?></td>
                        <td><?php echo $row['longtitude']; ?></td>
                        <td><?php echo $row['latitude']; ?></td>
                        <td><?php echo $row['description']; ?></td> 
                        <!-- <td><img src="<?php echo $row['image']; ?>" alt=""></td> -->
                        <td><?php echo $row['address']; ?></td>
                        <td>
                        <button tpye="submit" class="btn btn-sucess">Edit</button>
                        </td>
                     </tr>
                     <?php
                    }
                 }
                 else
                 {
                     echo "BOBO KA";
                 }
                
                     
                




                 ?>
                     
                        
                     
                 </tbody>
               </table>


          </div>

    



  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<?php include('includes/footer-content.php');?>

</div>
<!-- End of Content Wrapper -->

<?php include('footer.php');?>